package org.sbolstandard;

import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;

import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLWriter;
import org.sbolstandard.core2.RestrictionType;
import org.sbolstandard.core2.SequenceOntology;

public class SequenceConstraintOutput {
	public static void main( String[] args ) throws Exception
    {
		String prURI="http://partsregistry.org";
		//String prPrefix="pr";	
		SBOLDocument document = new SBOLDocument();		
		/*
		Sequence seq=document.createSequence(
				URI.create(prURI + "Part:BBa_J23119:Design"),
				 "ttgacagctagctcagtcctaggtataatgctagc", 
				URI.create("http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html")
				);
			*/	

		document.setDefaultURIprefix(prURI);
		document.setTypesInURIs(true);
		ComponentDefinition promoter = document.createComponentDefinition(
				"BBa_K174004",
				"",
				new HashSet<URI>(Arrays.asList(ComponentDefinition.DNA)));
		promoter.addRole(SequenceOntology.PROMOTER);
		
		promoter.setName("pspac promoter");
		promoter.setDescription("LacI repressible promoter");	
		
		ComponentDefinition constPromoter = document.createComponentDefinition(
				"pspac",
				"",
				new HashSet<URI>(Arrays.asList(ComponentDefinition.DNA)));
		constPromoter.addRole(SequenceOntology.PROMOTER);
		
		promoter.setName("constitutive promoter");
		promoter.setDescription("pspac core promoter region");	
		
		ComponentDefinition operator = document.createComponentDefinition(
				"LacI_operator",
				"",
				new HashSet<URI>(Arrays.asList(ComponentDefinition.DNA)));
				
		operator.addRole(SequenceOntology.OPERATOR);
		
		operator.setName("LacI operator");
		operator.setDescription("LacI binding site");	
		
		promoter.createSequenceConstraint(
				 "r1", 
				RestrictionType.PRECEDES, constPromoter.getIdentity(),operator.getIdentity() );
		
		//promoter.setSequence(seq.getIdentity());
		
		SBOLWriter.write(document,(System.out));		
    }
}
