package org.sbolstandard;

import javax.sound.midi.Sequence;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
        AnnotationOutput.main(null);
        CollectionOutput.main(null);
        ComponentDefinitionOutput.main(null);
        GenericTopLevelOutput.main(null);
        ModelOutput.main(null);
        ModuleDefinitionOutput.main(null);
        SBOLDocumentOutput.main(null);
        SequenceConstraintOutput.main(null);
        SequenceOutput.main(null);        
        SimpleComponentDefinitionExample.main(null);
           
    }
}
