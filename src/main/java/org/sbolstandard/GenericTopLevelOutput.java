package org.sbolstandard;


import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;

import javax.xml.namespace.QName;

import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.GenericTopLevel;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLWriter;
import org.sbolstandard.core2.SequenceOntology;

public class GenericTopLevelOutput {

	public static void main( String[] args ) throws Exception
    {
		String myAppURI="http://www.myapp.org";
		String myAppPrefix="myapp";
		String prURI="http://www.partsregistry.org";
				
		SBOLDocument document = new SBOLDocument();		
		document.addNamespace(URI.create(myAppURI+ "/") , myAppPrefix);
		
		document.setDefaultURIprefix(prURI);
		document.setTypesInURIs(true);
		
		GenericTopLevel topLevel=document.createGenericTopLevel(
				"datasheet1",
				"",
				new QName("http://www.myapp.org", "Datasheet", myAppPrefix)
				);
		topLevel.setName("Datasheet 1");
		
		topLevel.createAnnotation(new QName(myAppURI, "characterizationData", myAppPrefix), 
				URI.create(myAppURI + "/measurement/1"));
				

		topLevel.createAnnotation(new QName(myAppURI, "transcriptionRate", myAppPrefix), "1");			
		
		
		ComponentDefinition promoter = document.createComponentDefinition(
				"BBa_J23119",
				"",
				new HashSet<URI>(Arrays.asList(ComponentDefinition.DNA)));
		
		promoter.addRole(SequenceOntology.PROMOTER);
		promoter.setName("J23119");
		promoter.setDescription("Constitutive promoter");	
				
		promoter.createAnnotation(new QName(myAppURI, "datasheet", myAppPrefix), topLevel.getIdentity());
		promoter.setWasDerivedFrom(URI.create("http://www.partsregistry.org/Part:BBa_J23119"));		
		
		SBOLWriter.write(document,(System.out));
		
    }
}
