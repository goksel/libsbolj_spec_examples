package org.sbolstandard;

import java.net.URI;
import java.util.Iterator;
import java.util.Set;

import org.sbolstandard.core2.Collection;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLWriter;
import org.sbolstandard.core2.TopLevel;

public class CollectionOutput {

	public static void main( String[] args ) throws Exception
    {
		SBOLDocument document = new SBOLDocument();		
		//Collection col=document.createCollection(URI.create("http://parts.igem.org/Promoters/Catalog/Anderson"));
		
		document.setDefaultURIprefix("http://parts.igem.org/Promoters/Catalog");		
		document.setTypesInURIs(false);		
		Collection col=document.createCollection("Anderson","");
		
		col.setName("Anderson promoters");
		col.setDescription("The Anderson promoter collection");				
		col.addMember(URI.create("http://partsregistry.org/Part:BBa_J23119"));
		col.addMember(URI.create("http://partsregistry.org/Part:BBa_J23118"));	
				
		SBOLWriter.write(document,(System.out));
		
		Set<URI> members=col.getMemberURIs();
		for (Iterator<URI> it=members.iterator();it.hasNext();)
		{
			URI member=it.next();
			System.out.println(member);
		}
		
    }
}
