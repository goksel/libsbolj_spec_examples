package org.sbolstandard;

import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;

import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.Model;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLWriter;
import org.sbolstandard.core2.Sequence;
import org.sbolstandard.core2.SystemsBiologyOntology;

public class ModelOutput {
	public static void main( String[] args ) throws Exception
    {
		SBOLDocument document = new SBOLDocument();		
		
		document.setTypesInURIs(false);
		
		document.setDefaultURIprefix("http://www.sbolstandard.org/examples");
		
		Model model=document.createModel(
				"pIKE_Toggle_1",
				"",
				URI.create("http://virtualparts.org/part/pIKE_Toggle_1"),
				URI.create("http://identifiers.org/edam/format_2585"), 
				SystemsBiologyOntology.CONTINUOUS_FRAMEWORK);
				//URI.create("http://identifiers.org/biomodels.sbo/SBO:0000062"));
		model.setName("pIKE_Toggle_1 toggle switch");
			//TODO: Add the role URI.create("http://sbols.org/v2#module_model"));
						
		
		SBOLWriter.write(document,(System.out));		
    }
}
