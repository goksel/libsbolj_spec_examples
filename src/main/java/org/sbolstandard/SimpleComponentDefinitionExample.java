package org.sbolstandard;


import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;

import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLWriter;
import org.sbolstandard.core2.Sequence;
import org.sbolstandard.core2.SequenceOntology;

public class SimpleComponentDefinitionExample {
	public static void main( String[] args ) throws Exception
    {
		String prURI="http://partsregistry.org";
		
	
		SBOLDocument document = new SBOLDocument();		
		document.setDefaultURIprefix(prURI);
		document.setTypesInURIs(true);
		ComponentDefinition promoter = document.createComponentDefinition(
				"BBa_J23119",
				"",
				new HashSet<URI>(Arrays.asList(
						ComponentDefinition.DNA,
						URI.create("http://identifiers.org/chebi/CHEBI:4705")												
						)));
		promoter.addRole(SequenceOntology.PROMOTER);
		promoter.addRole(URI.create("http://identifiers.org/so/SO:0000613"));
				
		promoter.setName("J23119 promoter");
		promoter.setDescription("Constitutive promoter");	
		promoter.setWasDerivedFrom(URI.create("http://partsregistry.org/Part:BBa_J23119"));
	  
		document.setDefaultURIprefix(prURI);	
		Sequence seq=document.createSequence(
				"BBa_J23119",
				"",
				 "ttgacagctagctcagtcctaggtataatgctagc", 
				URI.create("http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html")
				);
		seq.setWasDerivedFrom(URI.create("http://parts.igem.org/Part:BBa_J23119:Design"));
		promoter.addSequence(seq.getIdentity());
		
		//TODO: http://www.purl.org/obo/CHEBI#CHEBI_24431
		/*document.setDefaultURIprefix("http://www.purl.org/obo/owl/CHEBI#");	
		ComponentDefinition component = document.createComponentDefinition(
				"CHEBI_24431",
				"",
				new HashSet<URI>(Arrays.asList(
						URI.create("http://www.biopax.org/release/biopax-level3.owl#SmallMolecule"))));
		
		component.setName("CHEBI:24431");
		*/
		SBOLWriter.write(document,(System.out));		
    }
}
